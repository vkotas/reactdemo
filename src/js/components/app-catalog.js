var React = require('react');
var AppStore = require('../stores/app-store.js');
var AddToCart = require('../components/app-addtocart.js');

function getCatalog() {
  return {items: AppStore.getCatalog()}
}

var Catalog =
  React.createClass({
    getInitialState: function() {
      return getCatalog();
    },
    render: function() { 
      var items = this.state.items.map(function(item, i){
        return <tr key={i}><td>{item.title}</td><td>${item.cost}</td><td><AddToCart item={item} /></td></tr>
      });

      return <table className="pure-table pure-table-bordered">
        <thead><tr><th>Title</th><th>Price</th><th></th></tr></thead>
          <tbody>
          {items}
          </tbody>
        </table>
    }
  });


module.exports = Catalog;
