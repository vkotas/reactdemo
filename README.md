# React & Flux demo application #
* Recommended video introduction: http://www.youtube.com/watch?v=i__969noyAM
* Collection of React awesomeness: https://github.com/enaqx/awesome-react

### How do I get set up? ###
* npm install
* gulp

### Credits ###
* https://egghead.io/series/react-flux-architecture

### Hate ###
* What's wrong with AngularJS: https://medium.com/este-js-framework/whats-wrong-with-angular-js-97b0a787f903

![a75X1Mz_460sa.gif](https://bitbucket.org/repo/5n48KG/images/3065716013-a75X1Mz_460sa.gif)
