'use strict';

var gulp = require('gulp'),
  browserify = require('gulp-browserify'),
  concat = require('gulp-concat'),
  connect = require('gulp-connect');

gulp.task('browserify', function(){
  return gulp.src('src/js/main.js')
  .pipe(browserify({transform: 'reactify'}))
  .pipe(concat('main.js'))
  .pipe(gulp.dest('dist/js'))
  .pipe(connect.reload());
});


gulp.task('copy', function(){
  return gulp.src('src/index.*')
  .pipe(gulp.dest('dist'))
  .pipe(connect.reload());
});

gulp.task('connect', function() {
    connect.server({
      root: 'dist',
      port: 8008,
      livereload: true
    });
});

gulp.task('build', ['browserify', 'copy']);

gulp.task('watch', function() {
  gulp.watch('src/**/*', ['build']);
});

gulp.task('default', ['connect', 'build', 'watch']);
